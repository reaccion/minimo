Download the following libraries:

RadioHead:
https://www.airspayce.com/mikem/arduino/RadioHead/

Adafruit NeoPixel Library:
https://github.com/adafruit/Adafruit_NeoPixel/archive/master.zip

Uncompress the content files on your Arduino/libraries folder.
